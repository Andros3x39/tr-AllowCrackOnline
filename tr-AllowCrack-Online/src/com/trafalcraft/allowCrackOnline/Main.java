package com.trafalcraft.allowCrackOnline;

import com.trafalcraft.allowCrackOnline.auth.ChangeMdp;
import com.trafalcraft.allowCrackOnline.auth.Login;
import com.trafalcraft.allowCrackOnline.auth.Register;
import com.trafalcraft.allowCrackOnline.cache.ManageCache;
import com.trafalcraft.allowCrackOnline.util.DatabaseManager;
import com.trafalcraft.allowCrackOnline.util.Metrics;
import com.trafalcraft.allowCrackOnline.util.Msg;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class Main extends Plugin {

    private static Main instance;
    private static Configuration config;
    private static Plugin plugin;
    private static DatabaseManager manager;
    private static ManageCache manageCache;
    private static boolean disabled = false;
    private static boolean debug = false;

    private static HashMap<String, Date> banList = new HashMap<String, Date>();

    public void onEnable() {
        instance = this;
        plugin = this;
        manageCache = new ManageCache();

        // loadConfig config
        if (!getDataFolder().exists())
            getDataFolder().mkdir();

        File file = new File(getDataFolder(), "config.yml");

        if (!file.exists()) {
            try (InputStream in = getResourceAsStream("config.yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        loadConfig();
        updateConfigFields();
        Msg.load();
        getLogger().info("Config initialized");
        if (getConfig().getBoolean("metrics")) {
            this.getLogger().info("Enabling Metrics");
            try {
                Metrics metrics = new Metrics(this);
                getLogger().info("Metrics loaded");
            } catch (Exception e) {
                getLogger().info("An error occured while trying to enable metrics. Skipping...");
            }
        } else {
            getLogger().info("metrics disable in config file");
        }

        if (getConfig().getBoolean("debug")) {
            debug = true;
        }

        // Connect to the database
        manager = new DatabaseManager(this,
                "jdbc:mysql://" + getConfig().get("database.host") + ":" + getConfig().getInt("database.port")
                        + "/" + getConfig().get("database.db") + "?useUnicode=true&characterEncoding=utf8",
                getConfig().get("database.user").toString(), getConfig().get("database.pass").toString());
        Connection db = manager.getConnection();
        if (db == null) {
            getLogger()
                    .severe("AllowCrackOnline is disabling. Please check your database settings in your config.yml");
            disabled = true;
            return;
        }

        // Initial database table setup
        try {
            db.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS `"
                    + getConfig().getString("database.prefix")
                    + "users` (`name` VARCHAR(10) NOT NULL, `pass` varchar(64), `lastIP` varchar(15), `lastAuth` "
                    + "varchar(16), PRIMARY KEY (`name`), UNIQUE(`name`), INDEX(`name`)) CHARACTER SET utf8");

            ResultSet rs = db.createStatement().executeQuery(
                    "SELECT COUNT(*) FROM `" + getConfig().getString("database.prefix") + "users`");
            ResultSet rs2 = db.createStatement()
                    .executeQuery("SELECT * FROM `" + getConfig().getString("database.prefix") + "users`");

            while (rs.next())
                if (rs.getInt(1) > 0) {
                    for (int i = 0; i < rs.getInt(1); i++) {
                        rs2.next();
                        manageCache.addPlayerCache(rs2.getString(1), rs2.getString(2),
                                rs2.getString(3),
                                rs2.getString(4));
                    }
                }
        } catch (SQLException e) {
            getLogger().severe("Unable to connect to the database. Disabling...");
            e.printStackTrace();
            return;
        }

        //initialise les Listener+command
        getProxy().getPluginManager().registerCommand(this, new ACCommand(this));
        getProxy().getPluginManager().registerCommand(this, new Register(this));
        getProxy().getPluginManager().registerCommand(this, new Login(this));
        getProxy().getPluginManager().registerCommand(this, new ChangeMdp(this));
        getProxy().getPluginManager().registerListener(this, new ACListener(this
                , Msg.NOT_ALLOWED_CRACKED_USER.toString()));
        getLogger().info("loaded");

    }

    public void onDisable() {
        for (ProxiedPlayer p : getProxy().getPlayers()) {
            p.disconnect(TextComponent.fromLegacyText(Msg.PLUGIN_DISABLE_KICK_MSG.toString()));
        }
    }

    public static boolean hisDisable() {
        return disabled;
    }

    public void sendDebugMsg(Class localClass, String debugMsg) {
        if (debug) {
            debugMsg = "\n===========[tr-AllowCrackOnline-DEBUG: " + localClass.getSimpleName() + "]===========\n" + debugMsg;
            debugMsg += "\n============================================";
            getLogger().info(debugMsg);
        }

    }

    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(
                    getConfig(), new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateConfigFields() {
        if (config.get("metrics") == null) {
            config.set("metrics", true);
            saveConfig();
        }
        if (config.get("debug") == null) {
            config.set("debug", false);
            saveConfig();
        }
        if (config.get("login.numberOfTryAllowed") == null) {
            config.set("login.numberOfTryAllowed", 3);
        }
        if (config.get("login.banTickIfToManyFail") == null) {
            config.set("login.banMinuteIfToManyFail", 15);
        }
        if (config.get("msg.adminCommands.ipRemovedFromBannedList") == null) {
            config.set("msg.adminCommands.ipRemovedFromBannedList", "$ip is no longer banned");
        }
        if (config.get("msg.adminCommands.listBannedIp") == null) {
            config.set("msg.adminCommands.listBannedIp", "The banned IPs are : ");
        }
    }

    void loadConfig() {
        try {
            config = ConfigurationProvider
                    .getProvider(YamlConfiguration.class).load(
                            new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static Connection getDatabase() {
        return manager.getConnection();
    }

    public static void setDisable(boolean disable) {
        disabled = disable;
    }

    public static Configuration getConfig() {
        return config;
    }

    public static Main getInstance() {
        return instance;
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    public static ManageCache getManageCache() {
        return manageCache;
    }

    public static boolean hisDebug() {
        return debug;
    }

    public static void addBanIP(String ip) {
        Date date = new Date();
        int MinuteToMilliSecond = 60000;
        date.setTime(date.getTime() + (getConfig().getInt("login.banMinuteIfToManyFail") * MinuteToMilliSecond));
        banList.put(ip, date);
    }

    public static Set<String> getBanIPList() {
        return banList.keySet();
    }

    public static long getRemainingIPBan(String ip) {
        Date date = new Date();
        Date endBanDate = banList.get(ip);
        if (endBanDate == null) {
            return 0;
        } else {
            long remainingTime = endBanDate.getTime() - date.getTime();
            if (remainingTime <= 0) {
                banList.remove(ip);
            }
            return remainingTime;
        }
    }

    public static void removeBanIp(String ip) {
        banList.remove(ip);
    }
}
