package com.trafalcraft.allowCrackOnline.cache;

import com.google.common.collect.Maps;
import com.trafalcraft.allowCrackOnline.Main;

import java.util.Collection;
import java.util.Map;

public class ManageCache {

    private final Map<String, PlayerCache> crackedPlayerCache = Maps.newHashMap();

    public void addPlayerCache(String name, String pass, String lastIP, String lastAuth) {
        if (!this.crackedPlayerCache.containsKey(name)) {
            PlayerCache pa = new PlayerCache(name, pass, lastIP, lastAuth);
            crackedPlayerCache.put(name, pa);
            String debug = "player " + name + " added to the cache with this information";
            debug += "\npass: " + pass;
            debug += "\nlastIP: " + lastIP;
            debug += "\nlastAuth: " + lastAuth;
            Main.getInstance().sendDebugMsg(this.getClass(), debug);
        }
    }

    public boolean contains(String p) {
        return this.crackedPlayerCache.containsKey(p);
    }

    public void removePlayerCache(String playerName) {
        if (this.crackedPlayerCache.containsKey(playerName)) {
            crackedPlayerCache.remove(playerName);
            Main.getInstance().sendDebugMsg(this.getClass(), "player " + playerName + " removed from the cache");
        }
    }

    public Map<String, PlayerCache> playerCacheList() {
        return crackedPlayerCache;
    }

    public Collection<PlayerCache> getAllPlayerCacheList() {
        return crackedPlayerCache.values();
    }

    public PlayerCache getPlayerCache(String p) {
        return crackedPlayerCache.get(p);
    }

}
